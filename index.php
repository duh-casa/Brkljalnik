<?php

/*

    Copyright 2016 Jure Sah <dustwolfy@gmail.com>

    This file is part of Brkljalnik.

    Brkljalnik is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Brkljalnik is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Brkljalnik.  If not, see <http://www.gnu.org/licenses/>.
    
    Please refer to the README file for additional information.

*/

require_once "html.php";
$doc = new html("Brkljalnik", array(
"bootstrap" => True,
"css" => "slog.css"
));

?>
<h1>Brkljalnik</h1>

<p>Pozdravljeni,<br>
<br>
Dobrodošli v <a href="http://bos.zrc-sazu.si/cgi/a03.exe?name=sskj_testa&expression=brkljati&hs=1" target="_blank">Brkljalniku</a>. Tu lahko podelite svoje ideje, znanje, material in voljo da skupaj nekaj dosežemo.<br>
<br>
Spodaj prosim izberite kaj bi.<br>
<br>
Stran je trenutno v eksperimentalni fazi. Če bo uspela, jo bomo dodelali.</p>

<table>
	<tbody>
 		<tr>
 			<td><a href="ideja.php"><div>Imam IDEJO<br><small>rad bi jo podelil.</small></div></a></td>
 			<td><a href="mojster.php"><div>Sem MOJSTER<br><small>ful sem pameten.</small></div></a></td>
 		</tr>
 		<tr>
 			<td><a href="ropotija.php"><div>Imam ROPOTIJO<br><small>ki išče uporabo.</small></div></a></td>
 			<td><a href="pomoc.php"><div>Rad bi POMAGAL<br><small>narediti kaj kul.</small></div></a></td>
 		</tr>
	</tbody>
</table>
