<?php

/*

    Copyright 2016 Jure Sah <dustwolfy@gmail.com>

    This file is part of Brkljalnik.

    Brkljalnik is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Brkljalnik is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Brkljalnik.  If not, see <http://www.gnu.org/licenses/>.
    
    Please refer to the README file for additional information.

*/

class slike {

 function nakljucna($id = False) {
  if($id === False) {
   $id = uniqid();
  }
  
  $local = "slike/random-".md5($id).".jpg";
  $i = 0;
  while(!$this->is_image($local) || $i > 10) {
   $i++;
   file_put_contents($local, fopen("http://placemi.com/96x96", 'r')); //http://stackoverflow.com/questions/3938534/download-file-to-server-from-url
  }
  
  if($i > 10) {
   unlink($local);
   return False;
  } else {
   return $local;
  }
  
 }
 
	//http://stackoverflow.com/questions/1581136/validate-that-a-file-is-a-picture-in-php
	private function is_image($path) {
	 if(file_exists($path)) {
		 $a = getimagesize($path);
		 $image_type = $a[2];

		 if(in_array($image_type , array(IMAGETYPE_GIF , IMAGETYPE_JPEG ,IMAGETYPE_PNG , IMAGETYPE_BMP))) {
		  return true;
		 }
		 return false;
		} else {
	  return false;
		}
 }

}

?>
