<?php

/*

    Copyright 2016 Jure Sah <dustwolfy@gmail.com>

    This file is part of Brkljalnik.

    Brkljalnik is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Brkljalnik is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Brkljalnik.  If not, see <http://www.gnu.org/licenses/>.
    
    Please refer to the README file for additional information.

*/

class kontakt {
	
	private $cookie;

	function __construct() {
		if(isset($_COOKIE["kontakt"])) {
			$this->cookie = $_COOKIE["kontakt"];
		} else {
			$this->cookie = False;
		}
	}
	
	function set($kontakt = False) {
		$this->cookie = $kontakt;
		if(trim($this->cookie) == "") {
			$this->cookie = False;
		}
		if($this->cookie !== False) {
			setcookie("kontakt", $this->cookie, 0, "/");
		} else {
			if(isset($_COOKIE["kontakt"])) {
				setcookie('kontakt', null, -1, "/");
				unset($_COOKIE["kontakt"]);
			}
		}
	}
	
	function get() {
		if($this->cookie === False) {
			return "";
		} else {
			return $this->cookie;
		}
	}

}

?>
