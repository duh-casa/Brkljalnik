<?php

/*

    Copyright 2016 Jure Sah <dustwolfy@gmail.com>

    This file is part of Brkljalnik.

    Brkljalnik is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Brkljalnik is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Brkljalnik.  If not, see <http://www.gnu.org/licenses/>.
    
    Please refer to the README file for additional information.

*/

require_once "captcha.php";
$c = new recaptcha();

require_once "html.php";
$doc = new html("Brkljalnik - Imam IDEJO", array(
	"bootstrap" => True,
	"css" => "slog.css",
	"js" => $c->js()
));

?>
<h1>Brkljalnik - Imam IDEJO</h1>
<a href="index.php">Nazaj</a>.
<?php 

require_once "galerija.php";
$g = new galerija();

require_once "kontakt.php";
$k = new kontakt();

if(isset($_POST["ideja"])) {
	if($c->verify()) {
		$k->set($_POST["kontakt"]);
		$g->set($_POST["ime"], $_POST["opis"], $_POST["kontakt"]);
	}
	$returned = $_POST;	
} else {
	$returned = array(
		"ime" => "",
		"opis" => "",
		"kontakt" => $k->get()
	);
}

?>
<h2>Vnesi novo idejo</h2>
<hr><br>
<form method="POST">
    Ime ideje:<br><input type="text" name="ime" placeholder="Nova blazno kul ideja" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nova blazno kul ideja'" value="<?php echo $returned['ime']; ?>" /><br>
    Opis ideje:<br><textarea type="text" name="opis" placeholder="Glej naslov" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Glej naslov'"><?php echo $returned['opis']; ?></textarea><br>
    Kontakt na katerega si dosegljiv:<br><input type="text" name="kontakt" value="<?php echo $returned['kontakt']; ?>" /><br><br>
    <?php echo $c->show(); ?><br>
    <input type="submit" name="ideja" value="Objavi" class="btn btn-primary">
</form>
<h2>Obstoječe ideje</h2>
<hr><br>
<?php
$g->getOkvircki(False);

	//TU IMAŠ ZGORAJ OBRAZEC ZA VNOS NOVE IDEJE, SPODAJ PA JE GALERIJA OBSTOJEČIH
	//ime ideje, opis in kontakt

?>
