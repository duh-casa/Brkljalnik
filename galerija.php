<?php

/*

    Copyright 2016 Jure Sah <dustwolfy@gmail.com>

    This file is part of Brkljalnik.

    Brkljalnik is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Brkljalnik is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Brkljalnik.  If not, see <http://www.gnu.org/licenses/>.
    
    Please refer to the README file for additional information.

*/

require_once "mysqli.php";
require_once "slike.php";

class galerija {

	private $db;
	private $slike;

	function __construct() {
		$this->db = new dblink();
		$this->slike = new slike();
	}
	 
	function getOkvircki($linki = True) {
	?>
	 	<div class="ideje">
	 		<?php 
	 			foreach($this->db->q("SELECT `id`, `ime` FROM `ideje`") as $o) {
	 				$this->getOkvircek($o["id"], $o["ime"], $linki);
	 			}
	 		?>
	 	</div>
	<?php
	}
	 
	function getOkvircek($id, $ime, $linki = True) {
	?>
		<?php if($linki) { ?><a href="?ideja=<?php echo rawurlencode($id); ?>"><?php } ?>
		 	<div class="ideja">
		 		<div class="ideja-slika"><img src="<?php echo $this->slike->nakljucna('ideja: '.$ime); ?>" alt="slikca"></div>
		 		<div class="ideja-ime"><?php echo $ime; ?></div>
		 	</div>
	 	<?php if($linki) { ?></a><?php } ?>
	<?php	 
	}
	
	function set($ime = "Nova ideja", $opis = "Opis ni vnešen", $kontakt = "Ni podan") {
		$this->db->q("
				INSERT INTO `ideje` (`ime`, `opis`, `kontakt`)
				VALUES ('".$this->db->e($ime)."', '".$this->db->e($opis)."', '".$this->db->e($kontakt)."')
		");
	}
	
	function getMojstrovanja($id = 0) {
		$ideja = $this->db->q("SELECT `ime`, `opis` FROM `ideje` WHERE `id` = '".$this->db->e($id)."' LIMIT 1");
		
		?>
			<div class="ideja-polna">
		 		<h2><?php echo $ideja[0]["ime"]; ?></h2>
		 		<p><?php echo $ideja[0]["opis"]; ?></p>
			</div>
			<hr>
			
			<div class="komentarji">
			<?php
			foreach($this->db->q("SELECT `komentar`, `kontakt` FROM `mojstrovanja` WHERE `ideja` ='".$this->db->e($id)."' ORDER BY `id` ASC") as $k) {
			$komentar = nl2br(preg_replace(
              "~[[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]~",
              "<a href=\"\\0\">\\0</a>", 
              $k["komentar"]));
			?>
				<div class="komentar"><?php echo $k["kontakt"]; ?>: <?php echo $komentar; ?><div>
				<hr>
			<?php
			}
			?>
			</div>
			<?php
	}
	
	function setMojstrovanje($id = 0, $komentar = "", $kontakt = "Ni podan") {
		$this->db->q("
				INSERT INTO `mojstrovanja` (`ideja`, `komentar`, `kontakt`)
				VALUES ('".$this->db->e($id)."', '".$this->db->e($komentar)."', '".$this->db->e($kontakt)."')
		");	
	}
	
	function getRopotija($id = 0) {
		
		?>

			<div class="ropotija">
			<?php
			foreach($this->db->q("SELECT `opis`, `kontakt` FROM `ropotija` WHERE `ideja` ='".$this->db->e($id)."'") as $k) {
			?>
			 	<div class="ropotija-kos">
			 		<div class="ropotija-slika"><img src="<?php echo $this->slike->nakljucna('ropotija: '.$k['opis']); ?>" alt="slikca"></div>
			 		<div class="ropotija-ime"><?php echo $k["kontakt"]; ?>: <?php echo $k["opis"]; ?></div>
			 	</div>
			<?php
			}
			?>
			</div>
			<?php
	}
	
	function setRopotija($id = 0, $opis = "", $kontakt = "Ni podan") {
		$this->db->q("
				INSERT INTO `ropotija` (`ideja`, `opis`, `kontakt`)
				VALUES ('".$this->db->e($id)."', '".$this->db->e($opis)."', '".$this->db->e($kontakt)."')
		");	
	}	

}

?>
