<?php

/*

    Copyright 2016 Jure Sah <dustwolfy@gmail.com>

    This file is part of Brkljalnik.

    Brkljalnik is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Brkljalnik is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Brkljalnik.  If not, see <http://www.gnu.org/licenses/>.
    
    Please refer to the README file for additional information.

*/

class recaptcha {

	private $siteKey;
	private $secretKey;

	function __construct($site = "", $secret = "") {
	
	 include "captchaLogin.php";
  foreach($credentials as $key => $value) {
   if(!isset(${$key}) || ${$key} == "") {
    ${$key} = $value;
   }
  }
	
		$this->siteKey = $site;
		$this->secretKey = $secret;
	}
	
	function js() {
		return 'https://www.google.com/recaptcha/api.js';
	}
	
	function verify() {
        $responseData = json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$this->secretKey.'&response='.$_POST['g-recaptcha-response']));
        return ($responseData->success === True);
	}
	
	function show() {
		?><div class="g-recaptcha" data-sitekey="<?php echo $this->siteKey; ?>"></div><?php
	}
	

}

?>
