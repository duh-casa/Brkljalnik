<?php

/*

    Copyright 2016 Jure Sah <dustwolfy@gmail.com>

    This file is part of Brkljalnik.

    Brkljalnik is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Brkljalnik is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Brkljalnik.  If not, see <http://www.gnu.org/licenses/>.
    
    Please refer to the README file for additional information.

*/

require_once "captcha.php";
$c = new recaptcha();

require_once "html.php";
$doc = new html("Brkljalnik - Imam ROPOTIJO", array(
	"bootstrap" => True,
	"css" => "slog.css",
	"js" => $c->js()
));

?>
<h1>Brkljalnik - Imam ROPOTIJO</h1>
<a href="index.php">Nazaj</a>.
<h2>Ideje</h2>
<p>Izberi eno od idej spodaj in dopiši katere materiale imaš za njeno izvedbo.</p>
<hr><br>
<?php

require_once "galerija.php";
$g = new galerija();

$g->getOkvircki();

	//TU JE GALERIJA IDEJ, KO KLIKNEŠ ENO IMAŠ OPCIJO DODAJ PREDMET
	//polja so: opis in kontakt + captcha

if(isset($_GET["ideja"])) {

	require_once "kontakt.php";
	$k = new kontakt();

	?><br><hr><br><?php

	$g->getMojstrovanja($_GET["ideja"]);
	
	?>
	<h2>Prispevaj material</h2>
	<hr><br>
	<?php 

	if(isset($_POST["material-oddan"])) {
		if($c->verify()) {
			$k->set($_POST["kontakt"]);
			$g->setRopotija($_REQUEST["ideja"], $_POST["opis"], $_POST["kontakt"]);
		}
		$returned = $_POST;	
	} else {
		$returned = array(
			"opis" => "",
			"kontakt" => $k->get()
		);
	}

	?>
	<form method="POST">
		Opis materiala ki ga imaš:<br><textarea type="text" name="opis" placeholder="Neka zadeva ne vem kaj." onfocus="this.placeholder = ''" onblur="this.placeholder = 'Neka zadeva ne vem kaj.'"><?php echo $returned['opis']; ?></textarea><br>
		Kontakt na katerega si dosegljiv:<br><input type="text" name="kontakt" value="<?php echo $returned['kontakt']; ?>" /><br><br>
		<?php echo $c->show(); ?><br>
		<input type="hidden" name="ideja" value="<?php echo $_REQUEST['ideja']; ?>">
		<input type="submit" name="material-oddan" value="Objavi" class="btn btn-primary">
	</form>

	<h2>Že prispevan material</h2>
	<hr><br>
	<?php

	$g->getRopotija($_REQUEST["ideja"]);
	
}

?>




