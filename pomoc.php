<?php

/*

    Copyright 2016 Jure Sah <dustwolfy@gmail.com>

    This file is part of Brkljalnik.

    Brkljalnik is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Brkljalnik is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Brkljalnik.  If not, see <http://www.gnu.org/licenses/>.
    
    Please refer to the README file for additional information.

*/

require_once "html.php";
$doc = new html("Brkljalnik - Rad bi POMAGAL", array(
"bootstrap" => True,
"css" => "slog.css"
));

?>
<h1>Brkljalnik - Rad bi POMAGAL</h1>
<a href="index.php">Nazaj</a>.
<h2>Ideje</h2>
<p>Avtomatika lahko naredi samo toliko. Poglej spodnje ideje, izberi eno, poveži se z ostalimi in naredite stvar!</p>
<hr><br>
<?php

require_once "galerija.php";
$g = new galerija();


if(isset($_POST["komentar-oddan"])) {
	if($c->verify()) {
		$g->setMojstrovanje($_REQUEST["ideja"], $_POST["komentar"], $_POST["kontakt"]);
	}
	$returned = $_POST;	
} else {
	$returned = array(
		"komentar" => "Jst pa mislm da to ne bo šlo.",
		"kontakt" => ""
	);
}


$g->getOkvircki();

?><br><hr><br><?php

if(isset($_GET["ideja"])) {

	$g->getMojstrovanja($_GET["ideja"]);
	$g->getRopotija($_GET["ideja"]);
	
}

?>
