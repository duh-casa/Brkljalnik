<?php

/*

    Copyright 2016 Jure Sah <dustwolfy@gmail.com>

    This file is part of Brkljalnik.

    Brkljalnik is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Brkljalnik is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Brkljalnik.  If not, see <http://www.gnu.org/licenses/>.
    
    Please refer to the README file for additional information.

*/

require_once "captcha.php";
$c = new recaptcha();

require_once "html.php";
$doc = new html("Brkljalnik - Sem MOJSTER", array(
	"bootstrap" => True,
	"css" => "slog.css",
	"js" => $c->js()
));

?>
<h1>Brkljalnik - Sem MOJSTER</h1>
<a href="index.php">Nazaj</a>.
<h2>Ideje</h2>
<p>Izberi eno od idej spodaj in pokomentiraj kako misliš da bi se jo lahko naredilo.</p>
<hr><br>
<?php

require_once "galerija.php";
$g = new galerija();

require_once "kontakt.php";
$k = new kontakt();

if(isset($_POST["komentar-oddan"])) {
	if($c->verify()) {
		$k->set($_POST["kontakt"]);
		$g->setMojstrovanje($_REQUEST["ideja"], $_POST["komentar"], $_POST["kontakt"]);
	}
	$returned = $_POST;	
} else {
	$returned = array(
		"komentar" => "",
		"kontakt" => $k->get()
	);
}


$g->getOkvircki();

?><br><hr><br><?php

if(isset($_REQUEST["ideja"])) {

	$g->getMojstrovanja($_REQUEST["ideja"]);
	
	?>
	<form method="POST">
		Tvoj komentar glede izvedbe:<br><textarea type="text" name="komentar" placeholder="Jst pa mislm da to ne bo šlo." onfocus="this.placeholder = ''" onblur="this.placeholder = 'Jst pa mislm da to ne bo šlo.'" ><?php echo $returned['komentar']; ?></textarea><br>
		Kontakt na katerega si dosegljiv:<br><input type="text" name="kontakt" value="<?php echo $returned['kontakt']; ?>" /><br><br>
		<?php echo $c->show(); ?><br>
		<input type="hidden" name="ideja" value="<?php echo $_REQUEST['ideja']; ?>">
		<input type="submit" name="komentar-oddan" value="Objavi" class="btn btn-primary">
	</form>
	
	<h2>Prispevan material</h2>
	<hr><br>
	<?php
	
	$g->getRopotija($_REQUEST["ideja"]);

}

?>
