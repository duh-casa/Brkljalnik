/*

    Copyright 2016 Jure Sah <dustwolfy@gmail.com>

    This file is part of Brkljalnik.

    Brkljalnik is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Brkljalnik is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Brkljalnik.  If not, see <http://www.gnu.org/licenses/>.
    
    Please refer to the README file for additional information.

*/

CREATE TABLE `ideje` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ime` varchar(255) DEFAULT 'Nova ideja',
  `opis` text,
  `kontakt` varchar(45) DEFAULT 'Ni podan',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

CREATE TABLE `mojstrovanja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ideja` int(11) DEFAULT '0',
  `komentar` text,
  `kontakt` varchar(45) DEFAULT 'Ni podan',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `ropotija` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ideja` int(11) DEFAULT '0',
  `opis` text,
  `kontakt` varchar(45) DEFAULT 'Ni podan',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

